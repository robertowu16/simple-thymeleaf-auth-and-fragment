package com.roberto.server.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.roberto" })
@EntityScan("com.roberto.models")
@EnableJpaRepositories("com.roberto.dao")
@EnableAutoConfiguration
public class ThymeleafAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafAuthApplication.class, args);
	}

}
