package com.roberto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roberto.models.Role;

@Repository("roleRepository")
public interface RoleDao extends JpaRepository<Role, Long> {
	Role findByRole(String role);

	Role findRoleById(int id);
}
