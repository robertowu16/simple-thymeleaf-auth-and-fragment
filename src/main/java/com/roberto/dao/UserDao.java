package com.roberto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roberto.models.User;

@Repository("userRepository")
public interface UserDao extends JpaRepository<User, Long> {
	User findByEmail(String email);
}
