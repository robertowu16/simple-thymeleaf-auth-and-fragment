package com.roberto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.roberto.dao.*;
import com.roberto.models.*;

@Service("userService")
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userRepository;

	@Autowired
	private RoleDao roleRespository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public void saveUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setActive(1);
		Role userRole = roleRespository.findRoleById(user.getId());
		user.setRole(userRole);
		userRepository.save(user);
	}
}
