package com.roberto.services;

import com.roberto.models.User;

public interface UserService {
	public User findUserByEmail(String email);

	public void saveUser(User user);
}
